.\" Copyright (c) 1999 Michael Merten <mikemerten@yahoo.com>
.\" Copyright (c) 2000-2003 Herbert Xu <herbert@debian.org>
.\" May be distributed under the terms of version 2 of the
.\" GNU GPL. See the LICENSE file included with this package.
.\" $Id: apt-move.8,v 1.35 2006/01/14 04:05:23 herbert Exp $
.TH APT\-MOVE 8 "2002 Oct 7th" "APT-MOVE"

.SH NAME
apt\-move \- move cache of Debian packages into a mirror hierarchy.

.SH SYNOPSIS

.B apt\-move
.RB [ \-c
.IR conffile ]
.RB [ \-d
.IR dist ]
.RB [ \-afqt ]
.I command


.SH DESCRIPTION

The
.B apt\-move
script is used to move a collection of Debian package files into a proper
archive hierarchy of the form
.B $LOCALDIR/pool/...
where
.B LOCALDIR
is specified in the configuration file.
It is intended as a tool to help manage the
.BR apt\-get (8)
file cache, but could be configured to work with any collection
of Debian packages.

.PP
Additionally, using the
.I sync
and
.I mirror
commands, you can build your own local mirror of portions of a selected
binary and/or source distribution.

.PP
Running
.B apt\-move
periodically will assist in managing the resulting partial mirror
by (optionally) removing obsolete packages and creating valid local
Packages.gz and Sources.gz.

.SS "Commands"
The following commands are accepted by
.BR apt\-move :

.TP
.B get \fR[ \fIdir \fR]
This generates the master files using Packages and Sources files from the
.BR apt (8)
cache.  The master files are used to keep track of what packages are
available, and where packages should be installed.  If
.I dir
is specified, it will be used in lieu of the
.B LISTSTATE
variable.

.TP
.B getlocal \fR[ \fIdir \fR]
This is an alias of get.  It may be removed in future releases.

.TP
.B move
Moves a collection of packages into the local mirror tree.  Uses existing
master files (see
.IR get )
to repair any mangling done to the package names.  Any packages that aren't
listed in the master files or are obsolete will be left in the file cache
directory.  Obsolete packages will also be copied into the archive but
they will be removed after the next delete operation.  In the these two
cases, the package is considered to have been skipped.

.TP
.B delete
Delete obsolete package files.  Configurable through the
.I DELETE
and
.I MAXDELETE
settings in the
.B /etc/apt\-move.conf
file (see the
.I CONFIGURATION
section below).  It also deletes any index files of packages that are
no longer in the archive.  This means that you can simply delete packages
from the archive without affecting its consistency as long as you run the
.B delete
command afterwards.

.TP
.B packages
Builds new local versions of Packages.gz and Sources.gz files.

.TP
.B fsck
Rebuilds all index files used to make Packages and Sources files and reprocess
all packages in the archive by calling
.B movefile
on them.  Use this if you are upgrading from an old version (<< 4.2) of
.BR apt-move .

.TP
.B update
This is an alias, equivalent to
.RI ' get
.I move
.I delete
.IR packages '.
This is the preferred method for moving package files from your
cache into a local mirror.

.TP
.B local
This is an alias, equivalent to
.RI ' move
.I delete
.IR packages '.

.TP
.B localupdate
This is an alias for update.  It may be removed in future releases.

.TP
.B mirror
This command automatically runs
.IR get ,
then uses
.B /usr/lib/apt-move/fetch
and
.BR apt-get (8)
to download any packages missing from your mirror.  The downloaded files will
be installed into the repository using
.IR move .
Finally, it runs
.I packages
and exits.
See the
.I DIST
and
.I PKGTYPE
settings in
.BR /etc/apt\-move.conf .
Before using this command, you need to set up a
.B $LOCALDIR/.exclude
file containing patterns to exclude
unwanted files from your mirror.  See the
.B SAMPLE.exclude
file for an example.  See also the
.I "Exclude file"
section of
.I NOTES
below.  Note that this command will only mirror packages for the architecture
that you are running on.  It will, however, mirror all source packages.

.TP
.B sync
Similar to the
.I mirror
function, but only gets the packages that are currently installed on
your system.  It uses
.BR dpkg (8)
.I \-\-get\-selections
to find out what files to download.  It will skip any files that
match one of the patterns in the
.B $LOCALDIR/.exclude
file (if it exists).
.B sync
will get the latest versions of the packages, regardless of the
version currently installed on your system (think about it).

.TP
.B exclude
This command is used to test your
.B $LOCALDIR/.exclude
pattern file.  It will
go through the master lists and print any file that matches one
of the patters in
.BR $LOCALDIR/.exclude .
This will show you exactly what
files you have EXCLUDED from your mirror.  The
.B \-t
(test) flag has no affect on this command.  This
uses your existing master files, and does not require an internet
connection.

.TP
.BI movefile \ files...
This command is similar to move.  Instead of moving files from
.IR FILECACHE ,
it will move the files specified on the command line.

.TP
\fBlistbin \fR[ \fBmirror \fR| \fBsync \fR| \fBrepo \fR]
This command prints a list of packages which may serve as the input to
mirrorbin or mirrorsrc.  If the argument is
.B mirror
or
.BR sync ,
it will produce the same lists that the
.B mirror
and
.B sync
commands use.  If the argument is
.BR repo ,
the list produced will contain the packages that are currently in the
apt-move repository.

.TP
.B mirrorbin
This command will fetch the list of packages specified on the standard input,
and place them into the archive in the same way as
.B mirror
does.

.TP
.B mirrorsrc
This commands acts like
.BR mirrorbin ,
except that it fetches source packages instead of binary ones.

.SS Options
The following options are available from the command line:

.TP
.B \-a
Process all packages.  By default, commands like
.B listbin
and
.B mirrorbin
only process packages that differ in version between the apt-move repository
and the archive being mirrored.  This option causes all packages to be
considered even if the apt-move repository already contains the latest version.

.TP
\fB\-c \fIconffile
Use
.I conffile
as the configuration file instead of
.BR /etc/apt\-move.conf .

.TP
\fB\-d \fIdist
Use
.I dist
as the default suite instead of the value of
.I DIST
from the configuration file.

.TP
.B \-f
Forces deletion of files even when the percentage of files to
delete exceeds the
.I MAXDELETE
setting.  This is useful if
.B apt\-move
aborts with an error saying that too many files would be deleted, and you
want to delete the files anyway.  (Use with caution.) If you get this error,
using
.B \-ft
will show you the complete list of files, so you can verify them before
you use
.BR \-f.

.TP
.B \-q
Suppresses normal output.  This option is useful when
.B apt\-move
is used in a non-interactive script.

.TP
.B \-t
Makes a 'test run' and reports what WOULD be done for
.B option
but does not modify any of the cache or mirror files.

.SH CONFIGURATION

Before using
.BR apt\-move ,
edit the
.B /etc/apt\-move.conf
file to match your local setup.  Always remember to use the
.I test
parameter after any change in your configuration to make sure it will
work like you want it to.  You may also want to set the
.I DELETE
option to
.I no
to turn off file deletes until everything else is working successfully.

.PP
The file is read as a Bourne shell script.  So you must obey the
syntactical rules of
.BR sh(1) .
In particular, values with spaces in them must be quoted with single
or double quotes.

.PP
The following settings are recognized by
.B apt-move
(shown here with their defaults):

.TP
.BR APTSITES= """debian.midco.net non-us.debian.org"""
Set this to the names of sites in your
.B /etc/apt/sources.list
that you wish to mirror.  The value
.B /all/
refers to all non-file URIs.

.TP
.BR LOCALDIR= /mirrors/debian
This is the full (absolute) path to your debian directory (the top of your
local mirror).

.TP
.BR DIST= stable
This is the default suite assigned to packages when the
.I Release
file is missing.  It does not have any effect on whether a suite is
stored in the archive.

.TP
.BR PKGTYPE= binary
Set this to your choice of:
.IR binary ,
.I source
or
.I both
to tell the
.IR mirror ,
.I sync
and
.I movefile
which type(s) of files to get.

.TP
.BR FILECACHE= /var/cache/apt/archives
The directory where your local cache of packages are. The default will work
for the
.BR apt-get (8)
packages, unless you've changed the configuration in
.BR /etc/apt/apt.conf .

.TP
.BR LISTSTATE= /var/lib/apt/lists
The directory to your local cache of Packages files. The default will work
for the
.BR apt-get (8)
packages, unless you've changed the configuration in
.BR /etc/apt/apt.conf .

.TP
.BR DELETE= no
Determines whether obsolete packages (packages not listed in the master file,
or packages that have been superceded in the repository) are to be removed.

.TP
.BR MAXDELETE= 20
Maximum percentage of files
.B apt\-move
is allowed to delete during a normal run.  Anything exceeding this will
produce an error and abort the script.  I added this as a precaution
so that you won't lose your entire mirror when a new distribution is
released.  You can override this (with caution) using the
.I \-f
parameter with
.BR apt\-move .

.TP
.BR COPYONLY= no
If this is set to yes, then
.B move
and
.B movefile
will copy instead of move.  That is, the originals will be left alone.

.TP
.BR PKGCOMP= gzip
This should be set to a space-separated list of compression formats that
apt-move should provide when generating Packages and Sources files.  The
possible values are
.BR none ,
.B gzip
and
.BR bzip2 .
With the current
.B apt
package you should include at least
.BR none ,
as otherwise
.B apt
will complain about missing files.

.TP
.BR CONTENTS= no
If this is set to yes, then
.B packages
will generate Contents files.

.TP
.BR GPGKEY= ""
If this is set to non-empty string, then
.B packages
will sign generated Release files with the specified key.  You
must install
.B gnupg
before enabling this option.

.PP
For the
.I sync
and
.I mirror
commands to function correctly, you need to list your
.B apt\-move
repository at the top of
.BR /etc/apt/sources.list
as a
.BR file\ URI .

.SH FILES

.TP
.B /usr/bin/apt\-move
The script.

.TP
.B /etc/apt\-move.conf
Configuration file for the script.

.TP
.B /usr/share/man/man8/apt\-move.8.gz
The manpage.

.TP
.B /tmp/MOVE_*
The temporary files created at runtime.

.TP
.B /usr/lib/apt-move/fetch
Utility to fetch files just like
.IR "apt-get install -d" .
Except that no dependency analysis is done.

.TP
.B .apt-move/*.{binary, source}.local
Put entries of local packages here.  The fields are
``package priority section source version task'' for the binary file, and
``package priority section version'' for the source file.  The
.B version
field may be set to a single dash to refer to the latest version in the
archive.  Blank lines and lines beginning with a hash are ignored.

.SH "SEE ALSO"

.BR dpkg (8),
.BR apt\-get (8)

.SH NOTES
.SS "Exclude file"
The
.I mirror
command uses a file in the
.B $LOCALDIR/
directory called '.exclude' which contains
exclude patterns that are applied against the files to be mirrored.
These patterns were created with the following limitation: they must work
the same with with
.BR grep (1),
after any '*' characters are removed.  Unless you're careful setting this up,
you'll get unexpected results.  Run
.RB "'" apt-move
.B \-t
.BR mirror "'"
first, to make sure you're getting the results you intended.  Another way to
verify your exclude file is the use the
.I exclude
command for
.B apt\-move
to print a list of files your are excluding from your mirror.  See the sample
 .exclude file (SAMPLE.exclude) for an example of an .exclude file.

.SS Mirroring
The
.B apt\-move
.B mirror
and
.B sync
commands
do not test for available disk space.  The current
potato (main binary) distribution is over 1Gb in size.  Add the sources to
that and it can eat up the space on a partition really fast. I would advise you
to put your mirror somewhere other than the root partition.  Set up your
exclude file and run
.B "apt\-move -t mirror"
and examine the result.

.SS "Code names"
Since
.B apt\-move
gets the suite names from
.I Release
files, which usually use the names
.IR stable ,
.I testing
and
.IR unstable ,
the suites in the repository are named accordingly.  You can simulate the
code names by creating symbolic links in the
.I dists
directory.  For example, in order to make
.I testing
equivalent to
.IR sarge ,
you could run
.BR "ln -s testing $LOCALDIR/dists/sarge" .
Alternatively, you could delete the
.I testing
subdirectory and run
.BR "ln -s sarge $LOCALDIR/dists/testing" .
This will cause future executions of the
.I get
operation to use
.I sarge
whenever it sees
.I testing
in the
.I Release
file.

.SH DIAGNOSTICS

.B apt\-move
may exit with one of the following error messages:

.TP
.B "Could not read configuration."
.B apt\-move
could not find the
.B /etc/apt-move.conf
file.  Run the install script.

.TP
.B "Could not create directory."
For some reason, a necessary directory could not be created.

.TP
.B "You failed to select a distribution."
You did not configure a
.I DIST
setting in /etc/apt-move.conf.

.TP
.B "You specified an invalid package type."
You can only use
.IR binary ,
.I source
or
.I both
for the PKGTYPE setting.

.TP
.B "No master files exist!"
You need to run
.B apt-\move
with the
.B get
command at least once in order to create the master files which determine
where packages are to be installed.

.TP
.B "bc calculation returned invalid result"
.B apt\-move
uses the
.BR bc (1)
program to determine when the number of files to delete will exceed the
.I MAXDELETE
setting in
.BR apt\-move.conf .
If you get this error, make sure that
.I MAXDELETE
is set to a number in the range of 1 to 100, without the % sign.  Otherwise
you need to report this as a bug.

.TP
.B "Too many files to delete!"
.B apt\-move
will report this error if the number of files to be deleted exceeds the
.I MAXDELETE
setting in
.BR apt\-move.conf .
You need to study the output to determine if this is normal (in which case
you can override this using the
.I force
parameter), or if its due to some drastic change on the mirror site (like
a new release) or possibly due to a partial download of the master
Packages.gz or Sources.gz file.

.TP
.B "Your current mirror directory is incompatible..."
You have just upgraded from an old version of apt-move.  Update your
configuration, then run
.I apt-move fsck
and finally remove
.BR .apt-move/ancient .

.TP
.B "Could not read Release files."
.B apt\-move
could not read the release files needed to build the master files.  Make sure
you have run
.I apt-get update
and try again.

.TP
.B "Failed to remove original files."
.B apt\-move
could not remove the original copies of files that have just entered the
.B apt\-move
archive.  Make sure that you have permission to delete those files.

.TP
.B "Please remove $LOCALDIR/backup."
You must remove
.B LOCALDIR/backup
before running the
.B fsck
command.

.TP
.B "Unknown DIST setting."
The value of
.I DIST
must match the
.B Archive
field in the
.B Release
file of the distribution that you are trying to mirror.

.TP
.B "Cannot find index files for APTSITES."
.B apt\-move
could not find any index files for the
.B get
operation.  You should either run
.IR "apt-get update" ,
or run
.B apt\-move
with
.B -f
if only the
.I *.local 
files have been changed.

.SH BUGS

The exclusion system was designed prior to the introduction to the package
pools.  Hence its content still relates to the old structure of the Debian
archive.  This is confusing and it should be replaced with a new exclusion
system.

.PP

The
.I DIST
variable does not control what is actually downloaded by the
.B fetch
utility.  It should have a similar effect to that of
.BR "apt-get -t" .

.SH AUTHOR

Michael Merten <mikemerten@yahoo.com>

Herbert Xu <herbert@debian.org>
