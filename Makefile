# Debian makefile for apt-move

CXXFLAGS = -g -O2 -Wall
LDLIBS = -lapt-pkg

DESTDIR =

PREFIX = $(DESTDIR)/usr
BINDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/share/man/man8
CONFDIR = $(DESTDIR)/etc
LIBDIR = $(PREFIX)/lib/apt-move
SHAREDIR = $(PREFIX)/share/apt-move

all: fetch

fetch: fetch.o
	$(CXX) -o $@ $(LDFLAGS) $^ $(LOADLIBS) $(LDLIBS)

clean:
	rm -f fetch *.o

install: fetch
	install -p apt-move $(BINDIR)
	install fetch $(LIBDIR)
	install del1 move3 pkg1 $(SHAREDIR)
	install -m 644 get[23] move[4-7] *.awk Contents.head $(SHAREDIR)
	cp -p apt-move.conf $(CONFDIR)
	cp -p apt-move.8 $(MANDIR)

.PHONY: all clean install
